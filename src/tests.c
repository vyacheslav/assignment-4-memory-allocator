#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"

enum constants {
    HEAP_SIZE = 4096,
    MEM_SIZE = 256,
    COUNT = 8
};

static void destroy(void* heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) {size}).bytes);
}

static void free_array(int8_t** array, size_t length) {
    for (size_t i=0; i<length; i++) {
        _free(array[i]);
    }
    _free(array);
}

static bool test1() {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap) return false;
    void* mem = _malloc(MEM_SIZE);
    if (!mem) {
        destroy(heap, HEAP_SIZE);
        return false;
    }
    debug_heap(stdout, heap);
    _free(mem);
    destroy(heap, HEAP_SIZE);
    return true;
}

static bool test2() {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap) return false;
    int8_t** memes = _malloc(sizeof(int8_t*) * COUNT);
    for(size_t i = 0; i < COUNT; i++) {
        void* mem = _malloc(MEM_SIZE);
        memes[i] = mem;
    }
    int8_t* deleted = memes[4];
    _free(memes[4]);
    memes[4] = _malloc(MEM_SIZE);
    if (memes[4] != deleted) {
        free_array(memes, COUNT);
        destroy(heap, HEAP_SIZE);
        return false;
    }
    debug_heap(stdout, heap);
    free_array(memes, COUNT);
    destroy(heap, HEAP_SIZE);
    return true;
}

static bool test3() {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap) return false;
    int8_t** memes = _malloc(sizeof(int8_t*) * COUNT);
    for(size_t i = 0; i < COUNT; i++) {
        void* mem = _malloc(MEM_SIZE);
        memes[i] = mem;
    }
    int8_t* deleted1 = memes[4];
    int8_t* deleted2 = memes[5];
    _free(memes[4]);
    _free(memes[6]);
    _free(memes[5]);
    _free(memes[7]);
    memes[4] = _malloc(MEM_SIZE);
    memes[5] = _malloc(MEM_SIZE);
    if (memes[4] != deleted1 || memes[5] != deleted2) {
        free_array(memes, COUNT);
        destroy(heap, HEAP_SIZE);
        return false;
    }
    debug_heap(stdout, heap);
    free_array(memes, COUNT);
    destroy(heap, HEAP_SIZE);
    return true;
}

static bool test4() {
    void* heap = heap_init(HEAP_SIZE);
    if (!heap) return false;
    void* mem = _malloc(HEAP_SIZE * 2);
    if (!mem) {
        _free(mem);
        destroy(heap, HEAP_SIZE);
        return false;
    }
    debug_heap(stdout, heap);
    _free(mem);
    destroy(heap, HEAP_SIZE);
    return true;
}

void test() {
    if (test1()) {
        printf("Test 1 passed\n");
    }
    else {
        printf("Test 1 failed!\n");
        return;
    }
    if (test2()) {
        printf("Test 2 passed\n");
    }
    else {
        printf("Test 2 failed!\n");
        return;
    }
    if (test3()) {
        printf("Test 3 passed\n");
    }
    else {
        printf("Test 3 failed!\n");
        return;
    }
    if (test4()) {
        printf("Test 4 passed\n");
    }
    else {
        printf("Test 4 failed!\n");
        return;
    }
    printf("All tests passed\n");
}